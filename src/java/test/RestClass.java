/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import javax.json.Json;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Ville
 */
@Path("restpath")
public class RestClass {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RestClass
     */
    public RestClass() {
    }

    /**
     * Retrieves representation of an instance of test.RestClass
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        return Json.createObjectBuilder()
                .add("message", "Hello Get!")
                .toString();
    }

    /**
     * PUT method for updating or creating an instance of RestClass
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String printHello(
            @FormParam("name") String name,
            @FormParam("age") int age) {
        String message = String
                .format("Hello %s, you are %d years old!", name, age);
        return Json.createObjectBuilder()
                .add("message", message)
                .build()
                .toString();
    }
}
